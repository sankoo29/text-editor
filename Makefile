all: project

project: main.o functions.o initdata.o
	cc main.o functions.o initdata.o -o project -lncurses

main.o: main.c
	cc -c main.c

functions.o: functions.c
	cc -c functions.c

initdata.o: initdata.c
	cc -c initdata.c

clean:
	rm -rf *o project
