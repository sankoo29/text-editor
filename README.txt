Title Of Project : Text Editor
Name Of Author:	Sanket Sunil Patil.
MIS-ID:	111703044.

Here are text editor buttons along with there functions:

Arrows up, down, left, right : movement of curser on screen.
Backspace : deletes one character before cursor.
Delete	:  deletes one character after cursor.
F6:	start Selection of text from cursor on screen.
F7:	End Selection of text at position of cursor.
Ctrl + c :Copies text to clipboard.
Ctrl + v :Pastes copied text(text in clipboard)at position of cursor.
Ctrl + f :Search required text in file(Write text to find)
		It will display that text on first line at position of cursor.
Ctrl + n :Searches for text next to first find by Ctrl + f.
Ctrl + r :Search and replace
		It will take two strings and replaces first occurance of string 1 
		by string 2.To replace next occurance again perform same operation.
F9:	Displays text in clipboard and text to find just to verify what to copy and
	what to find.
F3: Close file without saving.
F4: Save and Close File.	
