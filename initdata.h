#define MAX 1024

typedef struct node {
	char ch;
	struct node *prev , *next;
}node;

typedef struct san {
	node *head[MAX];
	int tab;
	int line_on_screen;
	int maxy;
	char clipboard[1024], find[30] , replace[30];
	int a, b , c, d ;
	int maxx[MAX];
}san;

void init(san *f);
void insert(san *f, char ch, int y , int x);
void deletec(san *f, int y , int x);
void backspace(san *f, int y , int x);
void insertline(san *f ,int y , int x);
void deleteline(san *f ,int y , int x);
