#include<stdio.h>
#include<ncurses.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include "initdata.h"
#include<ncurses.h>
#include<string.h>
#define ctrl(x)  ((x) & 0x1f )

void DStoFile(san *f, char str[30]);
void printDS(san *f , int y , int x);
void FiletoDS(san *f , char str[30]);
void paste (san *f ,int y ,int x);
void copy(san *f );
int find(san *f , int y , int x);

int main(int argc , char *argv[]) {
	initscr();
	noecho();
	//cbreak();
	raw();
	int x = 0 , y = 0 , m = 0 , n = 0, i;
	int ch , a , b ,c ,d;
	san f;
	init(&f);
	int fd;
	char string[200];
	keypad(stdscr, TRUE);
	//fd = open(argv[1] , O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	FiletoDS(&f , argv[1]);
	printDS(&f, -1 , -1);
	int x1 = 0, y1 = 0;
	getyx(stdscr, y1 , x1);
	//printw("XXX");
	refresh();
	while(1) {
		ch = getch();
		//printw("%d",sizeof(ch));
		getyx(stdscr, y , x);
		if(ch == KEY_LEFT) {
			if(x1 != 0)
				x1 = x1 - 1; 
			//move(y, x-1);
		}
		else if(ch == KEY_BACKSPACE) {
			getyx(stdscr , y , x);
			backspace(&f , y , x);
			if(x == 0 && f.line_on_screen == 0) continue;
			if(x == 0){
				y1 = y1 ;
				x1 = f.maxx[y + f.line_on_screen -1];
				if(f.line_on_screen != 0)
					f.line_on_screen--;
			}
			else {
				x1 = x1 - 1;
			}
			clear();
			printDS(&f, y , x);
			/*if(x == 0) {
				y = y-1;
				x = (f.maxx[y]);
			}*/
			f.a = f.b = -1;
			//move( y , x-1);
		}
		else if(ch == KEY_DC) {
			getyx(stdscr , y , x);
			deletec(&f , y ,x);
			clear();
			printDS(&f, y , x);
			//move( y , x);
		}
		else if( ch == 10) { //enter button
			ch = '\n';
			insert(&f , ch , y , x);
			getyx(stdscr, y , x);
			if( y == 21 || f.line_on_screen != 0 ) {
				f.line_on_screen ++;
			}
			else {
				y1 = y1 + 1;
			}			
			if(y == 21 || f.line_on_screen != 0 )
				insertline(&f , y + f.line_on_screen - 1, x);
			else
				insertline(&f , y + f.line_on_screen , x);
			clear();
			//if( y == 21 ) f.line_on_screen ++;
			printDS(&f , y , x);
			//y1 = y1 + 1;
			x1 = 0;
			f.a = f.b = -1;
			//move(y + 1 , 0);
		}
		else if( ch == KEY_RIGHT) {
			x1 = x1 + 1;
			//move(y  , x + 1);
		}
		else if( ch == KEY_UP ) {
			if(y1 != 0)
				y1 = y1 - 1;
			else if( f.line_on_screen > 0)
				{
				f.line_on_screen --;
				getyx(stdscr , y , x);
				clear();
				printDS(&f, y , x);
				refresh();
			}
			else
				continue;
			//move(y - 1 , x);
		}
		else if( ch == KEY_DOWN) {
			if(y1 < 22)
				y1 = y1 + 1;
			else if( f.line_on_screen + y1 < f.maxy )
				{
				f.line_on_screen ++;
				getyx(stdscr , y , x);
				clear();
				printDS(&f, y , x);
				refresh();
			}
			else
				continue;
			//move(y + 1, x);
		}
		else if(ch == ctrl('c')) {
			copy(&f);  // copy content between a , b and  c ,d to string 
		}
		else if(ch == ctrl('v')) {
			getyx(stdscr , y , x);
			
			int i = 1 ,a = y , b = x;
			char ch = f.clipboard[0];
			while(ch != '\0') {
				if(ch == '\n') {
					insert(&f , ch , a , b);
					a++;
					b  = 0;
				}
				else {
					insert(&f , ch , a , b);
					b++;
				}
				ch = f.clipboard[i++];
			}
			
			
			//paste(&f , y + f.line_on_screen ,x );   // add f -> clipboard to DS at y , x
			clear();
			printDS(&f , y , x);
			refresh();
		}
		else if(ch == ctrl('f') ) {		//find 
			move(22 , 2);
			int i = 0 , b;
			ch = getch();
			while(ch != 10) {
				f.find[i++] = ch;
				addch(ch);
				ch = getch();
			}
			f.find[i] = '\0';
			b  = find(&f, 0 , 0);
			clear();
			printDS(&f , y  ,x);
			x1 = b; y1 = 0;
			//move(0 , b);
			refresh();
		}
		else if(ch == ctrl('n')) {
			getyx(stdscr , y , x);
			b = find(&f , y + f.line_on_screen +1, x);
			clear();
			printDS(&f , y  ,x);
			x1 = b; y1 = 0;
		}
		else if(ch == ctrl('r')) {
			move(22 , 2);
			int i = 0 , b , x;
			ch = getch();
			while(ch != 10) {
				f.find[i++] = ch;
				addch(ch);
				ch = getch();
			}
			f.find[i] = '\0';
			move(22 , 25);
			i = 0;
			ch = getch();
			while(ch != 10) {
				f.replace[i++] = ch;
				addch(ch);
				ch = getch();
			}
			f.replace[i] = '\0';
			b = find(&f , 0, 0);
			x = strlen(f.find);
			clear();
			printDS(&f , y  ,x);
			move(0 , b + x + 1);
			while(x--){
				backspace(&f , 0 , x + b + 1);
			}
			i = 0;
			x = strlen(f.replace);
			while(x--) {
				insert(&f , f.replace[x] , 0 , b);
			}
			clear();
			printDS(&f , y  ,x);
			y1 = 0;
			x1 = 0;
			
		}
		else if(ch == KEY_F(4)) {		// close with saving to file
			DStoFile(&f , argv[1]);
			
			break;
		}
		else if(ch == KEY_F(3)) {		// close without saving to file
			//DStoFile(&f , argv[1]);
			break;
		}
		else if(ch == KEY_F(6)) {		//start selection means set a , b
			getyx(stdscr ,y ,x );		//insert(&f , 'x' , y , x);
			f.a = y + f.line_on_screen;
			f.b = x ;
		}
		else if(ch == KEY_F(7)) {
			if( f.a == -1 || f.b == -1) continue;
			getyx(stdscr , y , x);  //  	end selection set c ,d
			f.c = y + f.line_on_screen;	//insert(&f , 'y' , y , x);clear();printDS(&f , y , x);
			f.d = x ;
			//select(a , b , c , d);   
			//printDS(&f , y , x);
		}
		else if(ch == KEY_F(9)) { 		//show clipboard 
			clear();int i = 1;
			refresh();
			move(0 , 0);
			printw("clipboard: \n");
			ch = f.clipboard[0];
			while(ch != '\0'){
				printw("%c",ch);
				ch = f.clipboard[i++];
			}
			printw("find_text:\n\n");
			ch = f.find[0];
			while(ch != '\0'){
				printw("%c",ch);
				ch = f.find[i++];
			}
		}
		
		else {
			getyx(stdscr, y , x);
			f.a = f.b =-1;
			 insert(&f , ch , y , x);
			clear();
			printDS(&f, y , x);
			//if(p == 1) {
			//if(ch == '\n') move(y+1 ,0);
			/*else move(y , x + 1);}*/
			refresh();
			x1 = x1 + 1;
		}
		if(x1 > f.maxx[y1 + f.line_on_screen]) {
			x1 = f.maxx[y1+ f.line_on_screen];
		}
		else if(y1 >= f.maxy - 1) {
			y1 = f.maxy - 1;
		}
		move(y1 , x1);
		refresh();
	}
	refresh();
	
	endwin();
	return 0;
}


