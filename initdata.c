#include<stdio.h>
#include<stdlib.h>
#include "initdata.h"
#include<ncurses.h>
void init(san *f) {
	int i = 0;
	f -> maxy = 1;
	f -> line_on_screen = 0;
	for(i = 0; i < MAX;i++) {
		f->head[i] = NULL;
		f->maxx[i] = 0;
	} 
	f-> a = f -> b = f ->c = f -> d =  -1;
}

void insert(san *f, char ch, int y , int x) {
	node *tmp = (node *)malloc(sizeof(node));
	tmp -> next = NULL;
	tmp -> ch = ch;
	if(x > f->maxx[y + f->line_on_screen]) return ;
	node *p = f->head[y + f -> line_on_screen];
	if(ch != '\n')
		f->maxx[y + f->line_on_screen]++;
	int a = 0;
	if( p == NULL || x == 0) {
		tmp -> next = f -> head[y + f->line_on_screen];
		f -> head[y + f->line_on_screen] = tmp;
		return;
	}
	while(a < x - 1) {
		p = p -> next;
		a++;
	}
	tmp -> next = p -> next;
	p -> next = tmp;
	return ;
}

void deletec(san *f, int y , int x) {
	node *p = f -> head[y + f->line_on_screen];
	int i = 0;
	f->maxx[y + f->line_on_screen]--;	
	if(x == 1) {
		node *tmp = f -> head[y + f->line_on_screen];
		f -> head[y + f->line_on_screen] = f-> head[y + f->line_on_screen] ->next;
		free(tmp);
		return;
	}
	for(i = 0; i < x - 1; i++) {
		p = p -> next;
	}
	node *tmp = p -> next;
	p -> next = p -> next ->next;
	free(tmp);
}

void backspace(san *f, int y , int x) {
	node *p = f -> head[y + f->line_on_screen];
	node *q = f -> head[y + f ->line_on_screen - 1];
	int i , a , b , m;
	if(x == 0) {
		while(q -> next -> ch != '\n') {
			q = q -> next;
		}
		i = 0;
		m = y + f->line_on_screen;
		f->maxx[y + f->line_on_screen - 1] += f->maxx[y + f->line_on_screen ];
		q -> next = p;
		i = y + f ->line_on_screen;
		while(i < f -> maxy) {
			f -> head[i] = f -> head[i+1];
			i++;
		}
		f -> head[i] = NULL;
		while(m <= f -> maxy) {
			f -> maxx[m] = f -> maxx[m + 1];
			m++;
		}
		
		f -> maxy --;
		return;
	}
	f->maxx[y + f->line_on_screen]--;
	if(x == 1) {
		node *tmp = f -> head[y + f->line_on_screen];
		f -> head[y + f->line_on_screen] = f-> head[y + f->line_on_screen] ->next;
		free(tmp);
		return;
	}
	for(i = 0; i < x-2; i++) {
		p = p -> next;
	}
	node *tmp = p -> next;
	p -> next = p -> next ->next;
	free(tmp);
}

void insertline(san *f , int y , int x) {
	int i , j , a = f -> maxy;
	//printw("%d",f->maxy);
	if(y == a - 1) {
		(f -> maxy)++;
		return;
	}
	for(i = a  ; i > y  ; i--) {
		f -> head[i] = f -> head[i - 1];
		f -> maxx[i] = f -> maxx[i -1];
	}
	f ->head[i + 1] = NULL;
	node *p = f -> head[y ];
	for(j = 0; j < x ; j++) {
		p = p -> next;
	}
	f -> maxx[i + 1] = 0;
	node *q = p -> next;
	while(q){
		(f -> maxx[i + 1])++;
		(f -> maxx[i])--;
		q = q -> next;
	}
	f ->head[i + 1] = p -> next;
	p -> next = NULL;
	(f -> maxy)++;
}

void deleteline(san *f , int y , int x) {
	int i , j ,a = f -> maxy;
	node *p = f -> head[y - 1] ;
	while(p ) {
		p = p -> next;
	}
	p -> next = f -> head[y];
	for( i = y ; i <= a ; i++) {
		f -> head[i] = f -> head[i + 1]; 
	}
	f -> head[i] = NULL;
	(f -> maxy)--;
}
